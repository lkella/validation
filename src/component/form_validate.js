import React from "react";
import "./index.css";

/**
 * Represents Login form with validations
 */
class LoginComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      username_error: "",
      password_error: "",
    };
  }

  /**
   * When the input fields are changed
   */

  onChange = (e) => {
    let targetname = [e.target.name];
    this.setState({ [e.target.name]: e.target.value });
    // When input is given ,errors should not display
    if (targetname[0] === "username") {
      this.setState({ username_error: "" });
    } else if (targetname[0] === "password") {
      this.setState({ password_error: "" });
    } else {
      // some other input, do nothing
    }
  };

  /**
   * Helper function to validate the form upon form submit click
   * @returns true if form validation is succesful, false otherwise
   */
  formValidation = () => {
    // destructuring the state
    let { username, password, username_error, password_error } = this.state;

    let is_username_valid = false;
    let is_password_valid = false;

    // username validations
    if (username === "") {
      username_error = "Please fill this field";
    } else {
      if (!username.match("^(?=[a-zA-Z]{6,9}$)(?!.*[_.]{2})[^_.].*[^_.]$")) {
        username_error = "username is not valid";
      } else {
        username_error = "";
        is_username_valid = true;
      }
    }

    // password validations

    if (password === "") {
      password_error = "Please fill this field";
    } else {
      if (
        password.match("^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*W).*$")
      ) {
        password_error =
          "Password must contain 1 uppercase,1 lowercase,1 digit,1 special char";
      } else {
        password_error = "";
        is_password_valid = true;
      }
    }

    this.setState({ username_error, password_error });
    return is_username_valid && is_password_valid;
  };

  /**
   * When the user clicks on Submit
   * returns either error in respective fields or form submitted successfully
   */

  onSubmit = (e) => {
    e.preventDefault();
    const isValid = this.formValidation();
    if (isValid === true) {
      //send username and password to server
      alert("Form Submitted");
      console.log(this.state);
      this.setState({ username: "", password: "" });
    }
  };

  render() {
    const { username, password } = this.state;
    return (
      <>
        <div className="container">
          <div className="header">
            <h2>Login Here</h2>
          </div>
          <form className="form" id="myform" onSubmit={this.onSubmit}>
            <div className="form-control">
              <label htmlFor="username">username</label>
              <input
                type="text"
                placeholder="username Id"
                name="username"
                value={username}
                id="username"
                onChange={this.onChange}
              />
              <div className="error_message">{this.state.username_error}</div>
            </div>

            <div className="form-control">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                placeholder="Password"
                value={password}
                onChange={this.onChange}
              />
              <div className="error_message">{this.state.password_error}</div>
            </div>

            <button type="submit">Submit</button>
          </form>
        </div>
      </>
    );
  }
}
export default LoginComponent;
